let profile={
    registry:{
        "Wallet":"../Network/vars/profiles/vscode/wallets/registry.rsm.com",
        "CP":"../Network/vars/profiles/rsmchannel_connection_for_nodesdk.json"
    },
    property:{
        "Wallet":"../Network/vars/profiles/vscode/wallets/property.rsm.com",
        "CP":"../Network/vars/profiles/rsmchannel_connection_for_nodesdk.json"        
    },
    user:{
        "Wallet":"../Network/vars/profiles/vscode/wallets/user.rsm.com",
        "CP":"../Network/vars/profiles/rsmchannel_connection_for_nodesdk.json"        
    },
}
module.exports={profile}
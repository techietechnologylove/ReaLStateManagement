
function getOption() {
    selectElement = document.querySelector('#select1');
    output = selectElement.value;
    document.querySelector('.output').textContent = output;
}

function buttonStatus(){
    var loginstatus=localStorage.getItem("loginStatus");
    if(loginstatus == "login"){
        document.getElementById("login").style.display="none";
        document.getElementById("logout").style.display="block";
    }else{
        document.getElementById("login").style.display="block";
        document.getElementById("logout").style.display="none";
    }
}

function logout(event){
    localStorage.setItem("loginStatus","logout");
    window.location.href = "/";
    alert("Logout");
    
}

function login(event){
    
    var user=document.getElementById("userType").value;
    const readUserId = document.getElementById('name11').value;
    
    if(user == "RegOff"){
       
      localStorage.setItem("userType","RegOff");
      localStorage.setItem("loginStatus","login");
       window.location.href = "/registry";
       alert(user);
     
    }else if(user == "Citizen"){
        
        var data=fetchUser(readUserId);
        //alert(JSON.stringify(data));
      
        if(data != null){
            
            if(data.userId == readUserId){
                localStorage.setItem("userType","user");
                localStorage.setItem("loginStatus","login");
                alert(data.userId );
                window.location.href = "/user1";
                alert(data.userId );
            }else{
                alert("User is not registerd");
            }
            
        }else{
            alert("User is not registerd");
        }

    }else{
        alert("Please Select a User type");
    }
    
}



function addProperty(event) {
    event.preventDefault();
    console.log("addData Function")

    const PropertyId = document.getElementById('PropertyId1').value;
    const PropertyName = document.getElementById('PropertyName1').value;
    const PropertyValue1 = document.getElementById('PropertyValue1').value;
    
    if (PropertyId.length == 0 ) {
        alert("Please enter the data properly");

    }
    else {
        fetch('/createProperty', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ 
                PropertyId: PropertyId, 
                PropertyName :PropertyName, 
                PropertyValue1:PropertyValue1
            })
        }).then(function (response) {
            console.log(response);
            if (response.status == 200) {
                alert("Added a new Property");

            } else {
                alert("Error in processing request");
            }

        }).catch(function (err) {
            console.log(err);
            alert("Error in processing request");
        })
    }

}

function readData(event) {

    event.preventDefault();
    const readPropertyId = document.getElementById('readPropertyId').value;

    console.log(readPropertyId);

    if (readPropertyId.length == 0) {
        alert("Please enter the data properly");
    }
    else {
        fetch('/readProperty', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ PropertyId: readPropertyId })
        })
            .then(function (response) {
                console.log(response);
                if (response.status != 200) {
                    console.log(response.status)
                    // alert("Error in processing request");

                } else {
                    return response.json();
                }
            })
            .then(function (Propertydata) {
                dataBuf = Propertydata["Propertydata"]
                console.log(dataBuf)
                alert(dataBuf);
            })
            .catch(function (err) {
                console.log(err);
                alert("Error in processing request");
            })
    }
}

function queryAllProperty(){


   
        fetch('/queryAllProperty', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({  })
        })
            .then(function (response) {
                console.log(response);
                if (response.status != 200) {
                    console.log(response.status)
                    // alert("Error in processing request");

                } else {
                    return response.json();
                }
            })
            .then(function (Propertydata) {
                dataBuf = Propertydata["Propertydata"]
                console.log(dataBuf)
                localStorage.setItem("allProperty",dataBuf);
                //alert(dataBuf);
            })
            .catch(function (err) {
                console.log(err);
                alert("Error in processing request");
            })
    
}

function fetchAllProperty(){

    queryAllProperty();
     
    var allProperty=localStorage.getItem("allProperty");
    allProperty=JSON.parse(allProperty);
    return allProperty;

}

function getPropertyHistory(PropertyId){

  
    fetch('/getPropertyHistory', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ PropertyId: PropertyId })
    })
        .then(function (response) {
            console.log(response);
            if (response.status != 200) {
                console.log(response.status)
                // alert("Error in processing request");

            } else {
                return response.json();
            }
        })
        .then(function (Propertydata) {
            dataBuf = Propertydata["Propertydata"]
            console.log(dataBuf)
            localStorage.setItem("propertyHistory",dataBuf);
            //alert(dataBuf);
        })
        .catch(function (err) {
            console.log(err);
            alert("Error in processing request");
        })

}

function registerProperty(event) {
    event.preventDefault();
    console.log("addData Function")

    const PropertyId = document.getElementById('PropertyId3').value;
    const ownerId = document.getElementById('ownerId3').value;
    //console.log("aa"+PropertyId+ownerId);
//     var uex=localStorage.getItem("uex");
//    uex=JSON.stringify(uex);
//     alert(uex);
//       if(uex == "true"){
       

        fetch('/registerProperty', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ 
                PropertyId: PropertyId, 
                ownerId :ownerId
            })
        }).then(function (response) {
            console.log(response);
            if (response.status == 200) {
                alert("Property Registered Successfully");

            } else {
                alert("Error in processing request");
            }

        }).catch(function (err) {
            console.log(err);
            alert("Error in processing request");
        })
    
    //   }else{
    //     alert("User not exist.")
    //    return;
    // }
}

function queryAllPropertyByOwner(){

    var user=localStorage.getItem("actUser");
    user=JSON.parse(user);
    var ownerId=user.userId;

    fetch('/queryAllPropertyByOwner', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ ownerId:ownerId })
    })
        .then(function (response) {
            console.log(response);
            if (response.status != 200) {
                console.log(response.status)
                // alert("Error in processing request");

            } else {
                return response.json();
            }
        })
        .then(function (Propertydata) {
            dataBuf = Propertydata["Propertydata"]
            console.log(dataBuf)
            localStorage.setItem("ownerProperty",dataBuf);
            //alert(dataBuf);
        })
        .catch(function (err) {
            console.log(err);
            alert("Error in processing request");
        })
}

function fetchOwnerProperty(){

    queryAllPropertyByOwner();
    var property=localStorage.getItem("ownerProperty");
    property=JSON.parse(property);

    return property;
}

function auctionProperty(event) {
    event.preventDefault();
    console.log("addData Function")

    const PropertyId = document.getElementById('PropertyId4').value;
    const sellValue = document.getElementById('sellValue4').value;
    

        fetch('/auctionProperty', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ 
                PropertyId: PropertyId, 
                sellValue :sellValue
            })
        }).then(function (response) {
            console.log(response);
            if (response.status == 200) {
                alert("Property Auctioned Successfully");

            } else {
                alert("Error in processing request");
            }

        }).catch(function (err) {
            console.log(err);
            alert("Error in processing request");
        })
    

}

function queryAllAuctProperty(){


   
    fetch('/queryAllAuctProperty', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({  })
    })
        .then(function (response) {
            console.log(response);
            if (response.status != 200) {
                console.log(response.status)
                // alert("Error in processing request");

            } else {
                return response.json();
            }
        })
        .then(function (Propertydata) {
            dataBuf = Propertydata["Propertydata"]
            console.log(dataBuf)
            localStorage.setItem("allAuctProperty",dataBuf);
            //alert(dataBuf);
        })
        .catch(function (err) {
            console.log(err);
            alert("Error in processing request");
        })

}

function fetchAllAuctProperty(){

    queryAllAuctProperty();
 
var allProperty=localStorage.getItem("allAuctProperty");
allProperty=JSON.parse(allProperty);
return allProperty;

}

function buyProperty(PropertyId) {
    // event.preventDefault();
    // console.log("addData Function")

    var user=localStorage.getItem("actUser");
    user=JSON.parse(user);
    var ownerId=user.userId;
    
    

        fetch('/buyProperty', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ 
                PropertyId: PropertyId, 
                ownerId :ownerId
            })
        }).then(function (response) {
            console.log(response);
            if (response.status == 200) {
                alert("Property Baught Successfully");

            } else {
                alert("Error in processing request");
            }

        }).catch(function (err) {
            console.log(err);
            alert("Error in processing request");
        })
    

}

function addUser(event) {
    event.preventDefault();
    console.log("addData Function")

    const UserId = document.getElementById('UserId1').value;
    const UserName = document.getElementById('UserName1').value;
    
    if (UserId.length == 0 ) {
        alert("Please enter the data properly");

    }
    else {
        fetch('/createUser', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ 
                UserId: UserId, 
                UserName :UserName, 
            })
        }).then(function (response) {
            console.log(response);
            if (response.status == 200) {
                alert("Added a new User");

            } else {
                alert("Error in processing request");
            }

        }).catch(function (err) {
            console.log(err);
            alert("Error in processing request");
        })
    }

}

function readUser(readUserId) {

    // event.preventDefault();
    // const readUserId = document.getElementById('readUserId').value;

    // console.log(readUserId);

    if (readUserId.length == 0) {
        alert("Please enter the data properly");
    }
    else {
        fetch('/readUser', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ UserId: readUserId })
        })
            .then(function (response) {
                console.log(response);
                if (response.status != 200) {
                    console.log(response.status)
                    // alert("Error in processing request");

                } else {
                    return response.json();
                }
            })
            .then(function (Propertydata) {
                dataBuf = Propertydata["Propertydata"]
                console.log(dataBuf)
                localStorage.setItem("actUser",dataBuf);
                //alert(dataBuf);
            })
            .catch(function (err) {
                console.log(err);
                alert("Error in processing request");
            })
    }
}

function fetchUser(userId){

    //localStorage.removeItem("actUser");
    readUser(userId);
    var user=localStorage.getItem("actUser");
    user=JSON.parse(user);
   
    return user;
}

function queryAllUser(){


   
        fetch('/queryAllUsers', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({  })
        })
            .then(function (response) {
                console.log(response);
                if (response.status != 200) {
                    console.log("All Users")
                    console.log(response.status)
                    // alert("Error in processing request");

                } else {
                    return response.json();
                }
            })
            .then(function (Userdata) {
                dataBuf = Userdata["Userdata"]
                console.log(dataBuf)
                localStorage.setItem("allUser",dataBuf);
                //alert(dataBuf);
            })
            .catch(function (err) {
                console.log(err);
                alert("Error in processing request");
            })
    
}

function fetchAllUser(){

    queryAllUser();
     
    var allUser=localStorage.getItem("allUser");
    allUser=JSON.parse(allUser);
    return allUser;

}

function userExists(UserId) {


        fetch('/userExists', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ UserId: UserId })
        })
            .then(function (response) {
                console.log(response);
                if (response.status != 200) {
                    console.log(response.status)
                    // alert("Error in processing request");

                } else {
                    //localStorage.setItem("ueEx",response.json());
                    return response.json();
                }
            })
            .then(function (Propertydata) {
                dataBuf = Propertydata["Propertydata"]
                console.log(dataBuf)
                localStorage.setItem("uex",JSON.stringify(dataBuf));
                alert(dataBuf);
            })
            .catch(function (err) {
                console.log(err);
                alert("Error in processing request");
            })
    
}
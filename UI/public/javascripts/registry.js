function createNewProperty(){
    document.getElementById("createNewProperty").style.display="block";
    document.getElementById("displayProperties").style.display="none";
    document.getElementById("regProperty").style.display="none";
    document.getElementById("registerNewUser").style.display="none";
    document.getElementById("displayUsers").style.display="none";
    document.getElementById("propHistory1").style.display="none";
}

function displayProperties(){
    document.getElementById("createNewProperty").style.display="none";
    document.getElementById("displayProperties").style.display="block";
    document.getElementById("regProperty").style.display="none";
    document.getElementById("registerNewUser").style.display="none";
    document.getElementById("displayUsers").style.display="none";
    document.getElementById("propHistory1").style.display="none";

    var data =fetchAllProperty();

    const table = document.createElement("table");
    table.className="myTable";

// Create the table header row
const tableHeader = table.createTHead();
const headerRow = tableHeader.insertRow();

// Create the table headers
Object.keys(data[0].Record).forEach((key) => {
 
  const th = document.createElement("th");
  th.textContent = key;
  headerRow.appendChild(th);
});
  const th1 = document.createElement("th");
  th1.textContent = "Property History";
  headerRow.appendChild(th1);

// Create the table body rows
const tableBody = table.createTBody();
data.forEach((item) => {


  var tr =document.createElement("tr");

  Object.values(item.Record).forEach((value) => {

    var td=document.createElement("td");

    if(value == "ForSale"){
      if(item.Record.owner == "RegOffice"){

        var btn =document.createElement("button");
        btn.innerText="Register Property";
        btn.className="btn btn-primary";
        btn.onclick=function Func() {RegPropertyForm(item.Record.propertyId,item.Record.propertyName,item.Record.sellValue);}

        td.appendChild(btn);
        tr.appendChild(td);
      }else{
        var td=document.createElement("td");
        td.innerText=value;
        tr.appendChild(td);
      }
    }else{
     
      td.innerText=value;
      tr.appendChild(td);
    }
    
  });

  var td=document.createElement("td");
  var btn =document.createElement("button");
  btn.innerText="Property History";
  btn.className="btn btn-primary";
  btn.onclick=function Func() {getHistory(item.Record.propertyId);}
  td.appendChild(btn);
  tr.appendChild(td);

  tableBody.appendChild(tr);

});

// Append the table to the document body
document.getElementById("displayProperties").innerHTML="";
document.getElementById("displayProperties").appendChild(table);
}

function getHistory(propertyId){

   document.getElementById("createNewProperty").style.display="none";
    document.getElementById("displayProperties").style.display="none";
    document.getElementById("regProperty").style.display="none";
    document.getElementById("registerNewUser").style.display="none";
    document.getElementById("displayUsers").style.display="none";
    document.getElementById("propHistory1").style.display="block";
  
  getPropertyHistory(propertyId);
  var history15=localStorage.getItem("propertyHistory");
  history15=JSON.parse(history15);

  var userType =localStorage.getItem("userType");
  var cont;

  if(userType == "RegOff"){
      cont="propHistory1";
  }else{
    cont="propHistory2";
  }

  var data =history15;

  const table = document.createElement("table");
  table.className="myTable";

// Create the table header row
const tableHeader = table.createTHead();
const headerRow = tableHeader.insertRow();

// Create the table headers
Object.keys(data[0].Record).forEach((key) => {

const th = document.createElement("th");
th.textContent = key;
headerRow.appendChild(th);
});

// Create the table body rows
const tableBody = table.createTBody();
data.forEach((item) => {


var tr =document.createElement("tr");

Object.values(item.Record).forEach((value) => {

  var td=document.createElement("td");   
    td.innerText=value;
    tr.appendChild(td);
  
});
tableBody.appendChild(tr);
});

// Append the table to the document body
document.getElementById(cont).innerHTML="";
document.getElementById(cont).appendChild(table);

}
function getHistory2(propertyId){

  document.getElementById("disUserProf").style.display="none";
    document.getElementById("displayUserProperties").style.display="none";
    document.getElementById("buyPropertyForm").style.display="none";
    document.getElementById("PropertiesForSale").style.display="none";
    document.getElementById("auctProperty").style.display="none";
   document.getElementById("propHistory2").style.display="block";
 
 getPropertyHistory(propertyId);
 var history15=localStorage.getItem("propertyHistory");
 history15=JSON.parse(history15);

 var userType =localStorage.getItem("userType");
 var cont;

 if(userType == "RegOff"){
     cont="propHistory1";
 }else{
   cont="propHistory2";
 }

 var data =history15;

 const table = document.createElement("table");
 table.className="myTable";

// Create the table header row
const tableHeader = table.createTHead();
const headerRow = tableHeader.insertRow();

// Create the table headers
Object.keys(data[0].Record).forEach((key) => {

const th = document.createElement("th");
th.textContent = key;
headerRow.appendChild(th);
});

// Create the table body rows
const tableBody = table.createTBody();
data.forEach((item) => {


var tr =document.createElement("tr");

Object.values(item.Record).forEach((value) => {

 var td=document.createElement("td");   
   td.innerText=value;
   tr.appendChild(td);
 
});
tableBody.appendChild(tr);
});

// Append the table to the document body
document.getElementById(cont).innerHTML="";
document.getElementById(cont).appendChild(table);

}

function RegPropertyForm(propertyId ,propertyName, sellValue){

  document.getElementById("createNewProperty").style.display="none";
  document.getElementById("displayProperties").style.display="none";
  document.getElementById("regProperty").style.display="block";
  document.getElementById("registerNewUser").style.display="none";
  document.getElementById("displayUsers").style.display="none";

  //document.getElementById("regProperty").innerHTML=JSON.stringify(Record);
  document.getElementById("regProperty").innerHTML=

  `<h2>
    Register Property
  </h2>

  <form >
    <table>
        <tr>
            <td>Property Id</td>
            <td><input type="text" id="PropertyId3" value=`+propertyId+` readonly/></td>
        </tr>
        <tr>
            <td>Property Name</td>
            <td><input type="text" id="PropertyName3" value=`+propertyName+` readonly/></td>
        </tr>
        <tr>
            <td>Sell Value</td>
            <td><input type="text" id="sellValue3" value=`+sellValue+` readonly/></td>
        </tr>
        <tr>
            <td>Owner Id</td>
            <td><input type="text" id="ownerId3" /></td>
        </tr>
        <tr>
            <td></td>
            <td> <button onclick="registerProperty(event)">Register Property</button>
                <button type="reset">Clear</button>
            </td>
        </tr>
    </table>
 </form>`;
}

function registerNewUser(){
    document.getElementById("createNewProperty").style.display="none";
    document.getElementById("displayProperties").style.display="none";
    document.getElementById("regProperty").style.display="none";
    document.getElementById("registerNewUser").style.display="block";
    document.getElementById("displayUsers").style.display="none";
}

function displayUsers(){
    document.getElementById("createNewProperty").style.display="none";
    document.getElementById("displayProperties").style.display="none";
    document.getElementById("regProperty").style.display="none";
    document.getElementById("registerNewUser").style.display="none";
    document.getElementById("displayUsers").style.display="block";

    var data =fetchAllUser();
    const table = document.createElement("table");
    table.className="myTable";

// Create the table header row
const tableHeader = table.createTHead();
const headerRow = tableHeader.insertRow();

// Create the table headers
Object.keys(data[0].Record).forEach((key) => {
  const th = document.createElement("th");
  th.textContent = key;
  headerRow.appendChild(th);
});

// Create the table body rows
const tableBody = table.createTBody();
data.forEach((item) => {
  const row = tableBody.insertRow();

  Object.values(item.Record).forEach((value) => {
    const cell = row.insertCell();
    cell.textContent = value;
  });
});

// Append the table to the document body
document.getElementById("displayUsers").innerHTML="";
document.getElementById("displayUsers").appendChild(table);
}


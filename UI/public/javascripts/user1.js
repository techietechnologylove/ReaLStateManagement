function disUserProf(){

    document.getElementById("disUserProf").style.display="block";
    document.getElementById("displayUserProperties").style.display="none";
    document.getElementById("buyPropertyForm").style.display="none";
    document.getElementById("PropertiesForSale").style.display="none";
    document.getElementById("auctProperty").style.display="none";
    document.getElementById("propHistory2").style.display="none";

    var user=localStorage.getItem("actUser");
    user=JSON.parse(user);
    document.getElementById("disUserProf").innerHTML="";
    document.getElementById("disUserProf").innerHTML=
    `<h2>Profile</h2>
    <p><strong>User ID: </strong> `+user.userId +`</p>
    <p><strong>User Name: </strong> `+user.userName+ `</p>`;
}

function displayUserProperties(){

    document.getElementById("disUserProf").style.display="none";
    document.getElementById("displayUserProperties").style.display="block";
    document.getElementById("buyPropertyForm").style.display="none";
    document.getElementById("PropertiesForSale").style.display="none";
    document.getElementById("auctProperty").style.display="none";
    document.getElementById("propHistory2").style.display="none";


    var data =fetchOwnerProperty();

    const table = document.createElement("table");
    table.className="myTable";

// Create the table header row
const tableHeader = table.createTHead();
const headerRow = tableHeader.insertRow();

// Create the table headers
Object.keys(data[0].Record).forEach((key) => {
 
  const th = document.createElement("th");
  th.textContent = key;
  headerRow.appendChild(th);
});
  const th1 = document.createElement("th");
  th1.textContent = "Property History";
  headerRow.appendChild(th1);

// Create the table body rows
const tableBody = table.createTBody();
data.forEach((item) => {


  var tr =document.createElement("tr");

  Object.values(item.Record).forEach((value) => {

    var td=document.createElement("td");

    if(value == "Registered"){

        var btn =document.createElement("button");
        btn.innerText="Auction Property";
        btn.className="btn btn-primary";
        btn.onclick=function Func() {auctProperty(item.Record.propertyId,item.Record.propertyName,item.Record.currValue);}

        td.appendChild(btn);
        tr.appendChild(td);
    }else{
     
      td.innerText=value;
      tr.appendChild(td);
    }
    
  });

  var td=document.createElement("td");
  var btn =document.createElement("button");
  btn.innerText="Property History";
  btn.className="btn btn-primary";
  btn.onclick=function Func() {getHistory2(item.Record.propertyId);}
  td.appendChild(btn);
  tr.appendChild(td);

  tableBody.appendChild(tr);

});

// Append the table to the document body
document.getElementById("displayUserProperties").innerHTML="";
document.getElementById("displayUserProperties").appendChild(table);
}

function PropertiesForSale(){

    document.getElementById("disUserProf").style.display="none";
    document.getElementById("displayUserProperties").style.display="none";
    document.getElementById("buyPropertyForm").style.display="none";
    document.getElementById("PropertiesForSale").style.display="block";
    document.getElementById("auctProperty").style.display="none";
    document.getElementById("propHistory2").style.display="none";


    var data =fetchAllAuctProperty();

    const table = document.createElement("table");
    table.className="myTable";

// Create the table header row
const tableHeader = table.createTHead();
const headerRow = tableHeader.insertRow();

// Create the table headers
Object.keys(data[0].Record).forEach((key) => {
 
  const th = document.createElement("th");
  th.textContent = key;
  headerRow.appendChild(th);
});

const th1 = document.createElement("th");
  th1.textContent = "Buy Property";
  headerRow.appendChild(th1);

  const th2 = document.createElement("th");
  th2.textContent = "Property History";
  headerRow.appendChild(th2);

// Create the table body rows
const tableBody = table.createTBody();
data.forEach((item) => {


  var tr =document.createElement("tr");

  Object.values(item.Record).forEach((value) => {

    var td=document.createElement("td");
      td.innerText=value;
      tr.appendChild(td);
    
  });

  var td11=document.createElement("td");
  var btn11 =document.createElement("button");
  btn11.innerText="Buy Property";
  btn11.className="btn btn-primary";
  btn11.onclick=function Func() {buyProperty(item.Record.propertyId);}
  td11.appendChild(btn11);
  tr.appendChild(td11);

  var td=document.createElement("td");
  var btn =document.createElement("button");
  btn.innerText="Property History";
  btn.className="btn btn-primary";
  btn.onclick=function Func() {getHistory2(item.Record.propertyId);}
  td.appendChild(btn);
  tr.appendChild(td);

  tableBody.appendChild(tr);

});

// Append the table to the document body
document.getElementById("PropertiesForSale").innerHTML="";
document.getElementById("PropertiesForSale").appendChild(table);
}

function auctProperty(propertyId ,propertyName, currValue){

    document.getElementById("disUserProf").style.display="none";
    document.getElementById("displayUserProperties").style.display="none";
    document.getElementById("buyPropertyForm").style.display="none";
    document.getElementById("PropertiesForSale").style.display="none";
    document.getElementById("auctProperty").style.display="block";
    document.getElementById("propHistory2").style.display="none";


    document.getElementById("auctProperty").innerHTML=

    `<h2>
      Auction Your Property
    </h2>
  
    <form >
      <table>
          <tr>
              <td>Property Id</td>
              <td><input type="text" id="PropertyId4" value=`+propertyId+` readonly/></td>
          </tr>
          <tr>
              <td>Property Name</td>
              <td><input type="text" id="PropertyName4" value=`+propertyName+` readonly/></td>
          </tr>
          <tr>
              <td>Current Value</td>
              <td><input type="text" id="currValue4" value=`+currValue+` readonly/></td>
          </tr>
          <tr>
          <td>Sell Value</td>
          <td><input type="text" id="sellValue4" /></td>
      </tr>
          <tr>
              <td></td>
              <td> <button onclick="auctionProperty(event)">Auction Property</button>
                  <button type="reset">Clear</button>
              </td>
          </tr>
      </table>
   </form>`;
}
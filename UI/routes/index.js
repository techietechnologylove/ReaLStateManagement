var express = require('express');
var router = express.Router();

const { ClientApplication } = require('../../Client/client')
let RegistryClient = new ClientApplication();


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Real State Management' });
});

router.get('/registry', function(req, res, next) {
  res.render('registry', { title: 'Real State Management' });
});

router.get('/user1', function(req, res, next) {
  res.render('user1', { title: 'Real State Management' });
});

router.get('/login', function(req, res, next) {
  res.render('login', { title: 'Real State Management' });
});



router.post('/createProperty', function (req, res) {

  const PropertyId = req.body.PropertyId;
  const PropertyName = req.body.PropertyName;
  const PropertyValue1=req.body.PropertyValue1;

  RegistryClient.generateAndSubmitTxn(
    "Registry",
    "Admin",
    "rsmchannel",
    "KBA-RealStateMangement",
    "PropertyContract",
    "invokeTxn",
    "",
    "createProperty",
    PropertyId, PropertyName ,PropertyValue1
  )
    .then(message => {
      console.log("Message is ", message)
      res.status(200).send({ message: "Added Property" })
    }
    )
    .catch(error => {
      console.log("Some error Occured: ", error)
      res.status(500).send({ error: `Failed to Add`, message: `${error}` })
    });
});

router.post('/readProperty', async function (req, res) {
  const readPropertyId = req.body.PropertyId;

  RegistryClient.generateAndSubmitTxn(
    "Registry",
    "Admin",
    "rsmchannel",
    "KBA-RealStateMangement",
    "PropertyContract",
    "queryTxn",
    "",
    "readProperty",
    readPropertyId
  ).then(message => {
    console.log(message.toString());

    res.status(200).send({ Propertydata: message.toString() });
  }).catch(error => {

    res.status(500).send({ error: `Failed to Read`, message: `${error}` })
  });

})

router.post('/queryAllProperty', async function (req, res) {
  

  RegistryClient.generateAndSubmitTxn(
    "Registry",
    "Admin",
    "rsmchannel",
    "KBA-RealStateMangement",
    "PropertyContract",
    "queryTxn",
    "",
    "queryAllProperty",
    
  ).then(message => {
    console.log(message.toString());

    res.status(200).send({ Propertydata: message.toString() });
  }).catch(error => {

    res.status(500).send({ error: `Failed to Read`, message: `${error}` })
  });

})

router.post('/getPropertyHistory', async function (req, res) {
  
  const PropertyId = req.body.PropertyId;

  RegistryClient.generateAndSubmitTxn(
    "Registry",
    "Admin",
    "rsmchannel",
    "KBA-RealStateMangement",
    "PropertyContract",
    "queryTxn",
    "",
    "getPropertyHistory",
    PropertyId
  ).then(message => {
    console.log(message.toString());

    res.status(200).send({ Propertydata: message.toString() });
  }).catch(error => {

    res.status(500).send({ error: `Failed to Read`, message: `${error}` })
  });

})

router.post('/registerProperty', function (req, res) {

  const PropertyId = req.body.PropertyId;
  const ownerId = req.body.ownerId;

  RegistryClient.generateAndSubmitTxn(
    "Registry",
    "Admin",
    "rsmchannel",
    "KBA-RealStateMangement",
    "PropertyContract",
    "invokeTxn",
    "",
    "registerProperty",
    PropertyId, ownerId
  )
    .then(message => {
      console.log("Message is ", message)
      res.status(200).send({ message: "Added Property" })
    }
    )
    .catch(error => {
      console.log("Some error Occured: ", error)
      res.status(500).send({ error: `Failed to Add`, message: `${error}` })
    });
});

router.post('/queryAllPropertyByOwner', async function (req, res) {
  
  const ownerId = req.body.ownerId;

  RegistryClient.generateAndSubmitTxn(
    "Registry",
    "Admin",
    "rsmchannel",
    "KBA-RealStateMangement",
    "PropertyContract",
    "queryTxn",
    "",
    "queryAllPropertyByOwner",
    ownerId
  ).then(message => {
    console.log(message.toString());

    res.status(200).send({ Propertydata: message.toString() });
  }).catch(error => {

    res.status(500).send({ error: `Failed to Read`, message: `${error}` })
  });

})

router.post('/auctionProperty', function (req, res) {

  const PropertyId = req.body.PropertyId;
  const sellValue = req.body.sellValue;

  RegistryClient.generateAndSubmitTxn(
    "Registry",
    "Admin",
    "rsmchannel",
    "KBA-RealStateMangement",
    "PropertyContract",
    "invokeTxn",
    "",
    "auctionProperty",
    PropertyId, sellValue
  )
    .then(message => {
      console.log("Message is ", message)
      res.status(200).send({ message: "Added Property" })
    }
    )
    .catch(error => {
      console.log("Some error Occured: ", error)
      res.status(500).send({ error: `Failed to Add`, message: `${error}` })
    });
});

router.post('/queryAllAuctProperty', async function (req, res) {
  

  RegistryClient.generateAndSubmitTxn(
    "Registry",
    "Admin",
    "rsmchannel",
    "KBA-RealStateMangement",
    "PropertyContract",
    "queryTxn",
    "",
    "queryAllAuctProperty",
    
  ).then(message => {
    console.log(message.toString());

    res.status(200).send({ Propertydata: message.toString() });
  }).catch(error => {

    res.status(500).send({ error: `Failed to Read`, message: `${error}` })
  });

})

router.post('/buyProperty', function (req, res) {

  const PropertyId = req.body.PropertyId;
  const ownerId = req.body.ownerId;

  RegistryClient.generateAndSubmitTxn(
    "Registry",
    "Admin",
    "rsmchannel",
    "KBA-RealStateMangement",
    "PropertyContract",
    "invokeTxn",
    "",
    "buyProperty",
    PropertyId, ownerId
  )
    .then(message => {
      console.log("Message is ", message)
      res.status(200).send({ message: "Added Property" })
    }
    )
    .catch(error => {
      console.log("Some error Occured: ", error)
      res.status(500).send({ error: `Failed to Add`, message: `${error}` })
    });
});




router.post('/createUser', function (req, res) {

  const UserId = req.body.UserId;
  const UserName = req.body.UserName;

  RegistryClient.generateAndSubmitTxn(
    "Registry",
    "Admin",
    "rsmchannel",
    "KBA-RealStateMangement",
    "UserContract",
    "invokeTxn",
    "",
    "createUser",
    UserId, UserName
  )
    .then(message => {
      console.log("Message is ", message)
      res.status(200).send({ message: "Added Property" })
    }
    )
    .catch(error => {
      console.log("Some error Occured: ", error)
      res.status(500).send({ error: `Failed to Add`, message: `${error}` })
    });
});

router.post('/readUser', async function (req, res) {
  const readUserId = req.body.UserId;

  RegistryClient.generateAndSubmitTxn(
    "Registry",
    "Admin",
    "rsmchannel",
    "KBA-RealStateMangement",
    "UserContract",
    "queryTxn",
    "",
    "readUser",
    readUserId
  ).then(message => {
    console.log(message.toString());

    res.status(200).send({ Propertydata: message.toString() });
  }).catch(error => {

    res.status(500).send({ error: `Failed to Read`, message: `${error}` })
  });

})

router.post('/queryAllUsers', async function (req, res) {
  

  RegistryClient.generateAndSubmitTxn(
    "Registry",
    "Admin",
    "rsmchannel",
    "KBA-RealStateMangement",
    "UserContract",
    "queryTxn",
    "",
    "queryAllUsers",
    
  ).then(message => {
    console.log(message.toString());

    res.status(200).send({ Userdata: message.toString() });
  }).catch(error => {

    res.status(500).send({ error: `Failed to Read`, message: `${error}` })
  });

})

router.post('/userExists', async function (req, res) {
  const readUserId = req.body.UserId;

  RegistryClient.generateAndSubmitTxn(
    "Registry",
    "Admin",
    "rsmchannel",
    "KBA-RealStateMangement",
    "UserContract",
    "queryTxn",
    "",
    "userExists",
    readUserId
  ).then(message => {
    console.log(message.toString());

    res.status(200).send({ Propertydata: message.toString() });
  }).catch(error => {

    res.status(500).send({ error: `Failed to Read`, message: `${error}` })
  });

})


module.exports = router;

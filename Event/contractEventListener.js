const { EventListener } = require('./events')

let RegistryEvent = new EventListener();

RegistryEvent.contractEventListener("Registry", "Admin", "rsmchannel",
    "KBA-RealStateMangement", "PropertyContract", "addPropertyEvent");


RegistryEvent.contractEventListener("Registry", "Admin", "rsmchannel",
    "KBA-RealStateMangement", "PropertyContract", "addRegPropertyEventData");


RegistryEvent.contractEventListener("Registry", "Admin", "rsmchannel",
    "KBA-RealStateMangement", "PropertyContract", "addAuctPropertyEventData");


RegistryEvent.contractEventListener("Registry", "Admin", "rsmchannel",
    "KBA-RealStateMangement", "PropertyContract", "addBuyPropertyEventData");


 RegistryEvent.contractEventListener("Registry", "Admin", "rsmchannel",
    "KBA-RealStateMangement", "UserContract", "addUserEventData");

/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const RegistryContract = require('./lib/registry-contract');
const PropertyContract = require('./lib/property-contract');
const UserContract = require('./lib/user-contract');

module.exports.RegistryContract = RegistryContract;
module.exports.PropertyContract= PropertyContract;
module.exports.UserContract = UserContract;
module.exports.contracts = [ RegistryContract ,PropertyContract,UserContract];

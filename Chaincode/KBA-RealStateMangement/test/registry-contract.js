/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { ChaincodeStub, ClientIdentity } = require('fabric-shim');
const { RegistryContract } = require('..');
const winston = require('winston');

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.should();
chai.use(chaiAsPromised);
chai.use(sinonChai);

class TestContext {

    constructor() {
        this.stub = sinon.createStubInstance(ChaincodeStub);
        this.clientIdentity = sinon.createStubInstance(ClientIdentity);
        this.logger = {
            getLogger: sinon.stub().returns(sinon.createStubInstance(winston.createLogger().constructor)),
            setLevel: sinon.stub(),
        };
    }

}

describe('RegistryContract', () => {

    let contract;
    let ctx;

    beforeEach(() => {
        contract = new RegistryContract();
        ctx = new TestContext();
        ctx.stub.getState.withArgs('1001').resolves(Buffer.from('{"value":"registry 1001 value"}'));
        ctx.stub.getState.withArgs('1002').resolves(Buffer.from('{"value":"registry 1002 value"}'));
    });

    describe('#registryExists', () => {

        it('should return true for a registry', async () => {
            await contract.registryExists(ctx, '1001').should.eventually.be.true;
        });

        it('should return false for a registry that does not exist', async () => {
            await contract.registryExists(ctx, '1003').should.eventually.be.false;
        });

    });

    describe('#createRegistry', () => {

        it('should create a registry', async () => {
            await contract.createRegistry(ctx, '1003', 'registry 1003 value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1003', Buffer.from('{"value":"registry 1003 value"}'));
        });

        it('should throw an error for a registry that already exists', async () => {
            await contract.createRegistry(ctx, '1001', 'myvalue').should.be.rejectedWith(/The registry 1001 already exists/);
        });

    });

    describe('#readRegistry', () => {

        it('should return a registry', async () => {
            await contract.readRegistry(ctx, '1001').should.eventually.deep.equal({ value: 'registry 1001 value' });
        });

        it('should throw an error for a registry that does not exist', async () => {
            await contract.readRegistry(ctx, '1003').should.be.rejectedWith(/The registry 1003 does not exist/);
        });

    });

    describe('#updateRegistry', () => {

        it('should update a registry', async () => {
            await contract.updateRegistry(ctx, '1001', 'registry 1001 new value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1001', Buffer.from('{"value":"registry 1001 new value"}'));
        });

        it('should throw an error for a registry that does not exist', async () => {
            await contract.updateRegistry(ctx, '1003', 'registry 1003 new value').should.be.rejectedWith(/The registry 1003 does not exist/);
        });

    });

    describe('#deleteRegistry', () => {

        it('should delete a registry', async () => {
            await contract.deleteRegistry(ctx, '1001');
            ctx.stub.deleteState.should.have.been.calledOnceWithExactly('1001');
        });

        it('should throw an error for a registry that does not exist', async () => {
            await contract.deleteRegistry(ctx, '1003').should.be.rejectedWith(/The registry 1003 does not exist/);
        });

    });

});

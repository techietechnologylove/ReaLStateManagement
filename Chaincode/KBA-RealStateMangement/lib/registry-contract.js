/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');

class RegistryContract extends Contract {

    async registryExists(ctx, registryId) {
        const buffer = await ctx.stub.getState(registryId);
        return (!!buffer && buffer.length > 0);
    }

    async createRegistry(ctx, registryId, value) {
        const exists = await this.registryExists(ctx, registryId);
        if (exists) {
            throw new Error(`The registry ${registryId} already exists`);
        }
        const asset = { value };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(registryId, buffer);
    }

    async readRegistry(ctx, registryId) {
        const exists = await this.registryExists(ctx, registryId);
        if (!exists) {
            throw new Error(`The registry ${registryId} does not exist`);
        }
        const buffer = await ctx.stub.getState(registryId);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }

    async updateRegistry(ctx, registryId, newValue) {
        const exists = await this.registryExists(ctx, registryId);
        if (!exists) {
            throw new Error(`The registry ${registryId} does not exist`);
        }
        const asset = { value: newValue };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(registryId, buffer);
    }

    async deleteRegistry(ctx, registryId) {
        const exists = await this.registryExists(ctx, registryId);
        if (!exists) {
            throw new Error(`The registry ${registryId} does not exist`);
        }
        await ctx.stub.deleteState(registryId);
    }

}

module.exports = RegistryContract;

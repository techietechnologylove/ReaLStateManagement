/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');

class PropertyContract extends Contract {

    async propertyExists(ctx, propertyId) {
        const buffer = await ctx.stub.getState(propertyId);
        return (!!buffer && buffer.length > 0);
    }

    async createProperty(ctx, propertyId, propertyName ,currValue) {
        const exists = await this.propertyExists(ctx, propertyId);
        if (exists) {
            throw new Error(`The property ${propertyId} already exists`);
        }
        const asset = { 
            assetType: 'property',
            propertyId:propertyId,
            propertyName:propertyName,
            currValue:currValue,
            sellValue:currValue,
            owner:"RegOffice",
            status:"ForSale"

         };
        const buffer = Buffer.from(JSON.stringify(asset));
        let addPropertyEventData = { Type: 'Property creation', propertyName: propertyName };
        await ctx.stub.setEvent('addPropertyEvent', Buffer.from(JSON.stringify(addPropertyEventData)));
        await ctx.stub.putState(propertyId, buffer);
    }

    async readProperty(ctx, propertyId) {
        const exists = await this.propertyExists(ctx, propertyId);
        if (!exists) {
            throw new Error(`The property ${propertyId} does not exist`);
        }
        const buffer = await ctx.stub.getState(propertyId);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }

    async updateProperty(ctx, propertyId, newValue) {
        const exists = await this.propertyExists(ctx, propertyId);
        if (!exists) {
            throw new Error(`The property ${propertyId} does not exist`);
        }
        const asset = { value: newValue };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(propertyId, buffer);
    }

    async deleteProperty(ctx, propertyId) {
        const exists = await this.propertyExists(ctx, propertyId);
        if (!exists) {
            throw new Error(`The property ${propertyId} does not exist`);
        }
        await ctx.stub.deleteState(propertyId);
    }

    async registerProperty(ctx, propertyId, ownerId) {

        const exists = await this.propertyExists(ctx, propertyId);
        if (!exists) {
            throw new Error(`The property ${propertyId} does not exist`);
        }

        const asset=await this.readProperty(ctx, propertyId);
        asset.owner=ownerId;
        asset.status="Registered";

        const buffer = Buffer.from(JSON.stringify(asset));
        let addRegPropertyEventData = { Type: 'Property Registered', propertyId: propertyId , ownerId:ownerId};
        await ctx.stub.setEvent('addRegPropertyEventData', Buffer.from(JSON.stringify(addRegPropertyEventData)));
        await ctx.stub.putState(propertyId, buffer);
    }  

    async auctionProperty(ctx, propertyId, sellValue) {

        const exists = await this.propertyExists(ctx, propertyId);
        if (!exists) {
            throw new Error(`The property ${propertyId} does not exist`);
        }

        const asset=await this.readProperty(ctx, propertyId,sellValue);
        asset.sellValue=sellValue;
        asset.status="Auction";

        const buffer = Buffer.from(JSON.stringify(asset));
        let addAuctPropertyEventData = { Type: 'Property auctioned', propertyId: propertyId , sellValue:sellValue};
        await ctx.stub.setEvent('addAuctPropertyEventData', Buffer.from(JSON.stringify(addAuctPropertyEventData)));
        await ctx.stub.putState(propertyId, buffer);
    }  

    async buyProperty(ctx, propertyId, ownerId ) {

        const exists = await this.propertyExists(ctx, propertyId);
        if (!exists) {
            throw new Error(`The property ${propertyId} does not exist`);
        }

        const asset=await this.readProperty(ctx, propertyId);
        asset.owner=ownerId;
        asset.currValue=asset.sellValue;
        asset.status="Registered";

        const buffer = Buffer.from(JSON.stringify(asset));
        let addBuyPropertyEventData = { Type: 'Property Bought', propertyId: propertyId , ownerId:ownerId};
        await ctx.stub.setEvent('addBuyPropertyEventData', Buffer.from(JSON.stringify(addBuyPropertyEventData)));
        await ctx.stub.putState(propertyId, buffer);
    }  

    async getAllResults(iterator, isHistory) {
        let allResult = []
        for (let res = await iterator.next(); !res.done; res = await iterator.next()) {
            if (res.value && res.value.value.toString()) {
                let jsonRes = {}
                if (isHistory && isHistory == true) {
                    jsonRes.TxId = res.value.txId
                    jsonRes.TimeStamp = res.value.timestamp
                    jsonRes.Record = JSON.parse(res.value.value.toString())

                }
                else {
                    jsonRes.Key = res.value.key
                    jsonRes.Record = JSON.parse(res.value.value.toString())
                }
                allResult.push(jsonRes)

            }
        }
        await iterator.close()
        return allResult
    }

    async queryAllProperty(ctx) {
        const queryString = {
            selector: {
                assetType: 'property'
            },

        }
        let resultIterator = await ctx.stub.getQueryResult(JSON.stringify(queryString))
        let result = await this.getAllResults(resultIterator, false)
        return JSON.stringify(result)
    }

    async queryAllPropertyByOwner(ctx ,userId) {
        const queryString = {
            selector: {
                owner: userId
            },

        }
        let resultIterator = await ctx.stub.getQueryResult(JSON.stringify(queryString))
        let result = await this.getAllResults(resultIterator, false)
        return JSON.stringify(result)
    }

    async queryAllAuctProperty(ctx) {
        const queryString = {
            selector: {
                status:"Auction"
            },

        }
        let resultIterator = await ctx.stub.getQueryResult(JSON.stringify(queryString))
        let result = await this.getAllResults(resultIterator, false)
        return JSON.stringify(result)
    }

    async getPropertyHistory(ctx, propertyId) {
        let resultIterator = await ctx.stub.getHistoryForKey(propertyId)
        let result = await this.getAllResults(resultIterator, true)
        return JSON.stringify(result)
    }

    

}

module.exports = PropertyContract;

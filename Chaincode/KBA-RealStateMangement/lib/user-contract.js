/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');

class UserContract extends Contract {

    async userExists(ctx, userId) {
        const buffer = await ctx.stub.getState(userId);
        return (!!buffer && buffer.length > 0);
    }

    async createUser(ctx, userId, userName) {
        const exists = await this.userExists(ctx, userId);
        if (exists) {
            throw new Error(`The user ${userId} already exists`);
        }
        const asset = { 
            assetType: 'user',
            userId:userId,
            userName:userName
         };
        const buffer = Buffer.from(JSON.stringify(asset));
        let addUserEventData = { Type: 'User creation', userName: userName};
        await ctx.stub.setEvent('addUserEventData', Buffer.from(JSON.stringify(addUserEventData)));
        await ctx.stub.putState(userId, buffer);
    }

    async readUser(ctx, userId) {
        const exists = await this.userExists(ctx, userId);
        if (!exists) {
            throw new Error(`The user ${userId} does not exist`);
        }
        const buffer = await ctx.stub.getState(userId);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }

    async updateUser(ctx, userId, newValue) {
        const exists = await this.userExists(ctx, userId);
        if (!exists) {
            throw new Error(`The user ${userId} does not exist`);
        }
        const asset = { value: newValue };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(userId, buffer);
    }

    async deleteUser(ctx, userId) {
        const exists = await this.userExists(ctx, userId);
        if (!exists) {
            throw new Error(`The user ${userId} does not exist`);
        }
        await ctx.stub.deleteState(userId);
    }

    async getAllResults(iterator, isHistory) {
        let allResult = []
        for (let res = await iterator.next(); !res.done; res = await iterator.next()) {
            if (res.value && res.value.value.toString()) {
                let jsonRes = {}
                if (isHistory && isHistory == true) {
                    jsonRes.TxId = res.value.txId
                    jsonRes.TimeStamp = res.value.timestamp
                    jsonRes.Record = JSON.parse(res.value.value.toString())

                }
                else {
                    jsonRes.Key = res.value.key
                    jsonRes.Record = JSON.parse(res.value.value.toString())
                }
                allResult.push(jsonRes)

            }
        }
        await iterator.close()
        return allResult
    }

    async queryAllUsers(ctx) {
        const queryString = {
            selector: {
                assetType: 'user'
            },

        }
        let resultIterator = await ctx.stub.getQueryResult(JSON.stringify(queryString))
        let result = await this.getAllResults(resultIterator, false)
        return JSON.stringify(result);
    }

    async getUserHistory(ctx, userId) {
        let resultIterator = await ctx.stub.getHistoryForKey(userId)
        let result = await this.getAllResults(resultIterator, true)
        return JSON.stringify(result)
    }
}

module.exports = UserContract;
